define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'seer_payment',
                component: 'Seer_Payment/js/view/payment/method-renderer/custompayment'
            }
        );
        
        return Component.extend({});
    }
);