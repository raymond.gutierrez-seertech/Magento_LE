define(
    [
        'jquery',
        'Magento_Payment/js/view/payment/cc-form'
    ],
    function ($, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Seer_Payment/payment/custompayment'
            },
            context: function() {
                return this;
            },
            getCode: function() {
                return 'seer_payment';
            },
            isActive: function() {
                return true;
            }
        });
    }
);